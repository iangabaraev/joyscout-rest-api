from flask import Flask, Response
from flask_restful import Resource, Api, reqparse
from playstation_homepage import PlayStationStoreWebsite
from store_conf import APP_PARAMETERS
import json
import hashlib

app = Flask(__name__)
api = Api(app)


class PSPLUSGAMES(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        "hashed",
        str,
        required=False
    )
    def get(self):
        data = PSPLUSGAMES.parser.parse_args()
        result = dict()
        links = PlayStationStoreWebsite().ps_plus_deals()
        if data["hashed"]:
            result['PS Plus Free Games'] = [hashlib.md5(link.encode("utf-8")).hexdigest()
                                            for link in links]
            return Response(json.dumps(result, ensure_ascii=False), mimetype='application/json')
        result['PS Plus Free Games'] = list(links)
        return Response(json.dumps(result, ensure_ascii=False), mimetype='application/json')


class ALLLINKS(Resource):
    def get(self):
        links = PlayStationStoreWebsite().get_all_links()
        result = {'All games': list(links)}
        return Response(json.dumps(result, ensure_ascii=False), mimetype='application/json')


class ALLNEWGAMES(Resource):
    def get(self):
        links = PlayStationStoreWebsite().get_all_new_games_links()
        result = {'All new games': list(links)}
        return Response(json.dumps(result, ensure_ascii=False), mimetype='application/json')


api.add_resource(PSPLUSGAMES, '/api/store/ps-plus-games')
#api.add_resource(ALLLINKS, '/api/store/all-links')
api.add_resource(ALLNEWGAMES, '/api/store/all-new-games')


if __name__ == "__main__":
    app.run(port=APP_PARAMETERS["port"])
