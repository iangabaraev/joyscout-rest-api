# Production version

from flask import Flask
from flask import Response
from flask_restful import Api, Resource, reqparse
from sqlalchemy import Column, Boolean, String, Integer, DateTime, ARRAY
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func
from sqlalchemy import create_engine, and_
from sqlalchemy.ext.declarative import declarative_base
from joyscout_conf import APP_PARAMETERS, EXTERNAL, genre_map
import json
import requests

app = Flask(__name__)
api = Api(app)
engine = create_engine(APP_PARAMETERS['db'], echo=False)
Base = declarative_base()
restful_specs = EXTERNAL['restful specs']


def get_full_info(item, limit_to=None):
    columns = Catalogue.__table__.columns.keys()
    rows = [getattr(item, c) for c in columns]
    if not limit_to:
        return dict(zip(columns, rows))
    return {item: value for (item, value) in
            dict(zip(columns, rows)).items()
            if item in limit_to}


class TBR(Base):
    __tablename__ = 'tbr'
    id = Column(Integer, primary_key=True)
    link = Column(String, unique=True)
    hashcode = Column(String, unique=True)


class Free(Base):
    __tablename__ = 'f2p'
    id = Column(Integer, primary_key=True)
    link = Column(String, unique=True)
    hashcode = Column(String, unique=True)


class Tops(Base):
    __tablename__ = 'tops'
    id = Column(Integer, primary_key=True)
    link = Column(String, unique=True)
    hashcode = Column(String, unique=True)


class Catalogue(Base):
    __tablename__ = 'catalogue_of_games'
    id = Column(Integer, primary_key=True)
    link = Column(String, unique=True)
    hashcode = Column(String, unique=True)
    title = Column(String)
    publisher = Column(String)
    category = Column(String)
    price = Column(Integer)
    former_price = Column(Integer)
    lowest_price = Column(Integer)
    highest_price = Column(Integer)
    on_sale = Column(Boolean)
    ps_plus = Column(String)
    language = Column(ARRAY(String))
    genre = Column(ARRAY(String))
    size = Column(String)
    age = Column(Integer)
    cover = Column(String)
    screenshots = Column(ARRAY(String))
    description = Column(String)
    updated = Column(DateTime)


class StoreDB:
    Session = sessionmaker(bind=engine)
    session = Session()


class ToBeReleased(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'limit',
        type=int,
        required=False
    )
    def get(self):
        data = ToBeReleased.parser.parse_args()
        session = StoreDB.session
        result = dict()
        if not data['limit']:
            for item in session.query(TBR).all():
                instance = session.query(Catalogue).filter(
                    Catalogue.hashcode == item.hashcode).first()
                result[instance.title] = get_full_info(instance, ['publisher', 'price', 'former_price',
                                                                  'hashcode', 'on_sale',
                                                                  'ps_plus', 'cover'])
        else:
            for item in session.query(TBR).limit(data['limit']).all():
                instance = session.query(Catalogue).filter(
                    Catalogue.hashcode == item.hashcode).first()
                result[instance.title] = get_full_info(instance, ['publisher', 'price', 'former_price',
                                                                  'hashcode', 'on_sale',
                                                                  'ps_plus', 'cover'])
        if result:
            return Response(json.dumps(result, ensure_ascii=False), mimetype='application/json')
        return {'Message': 'Nothing found'}, 404


class FreeToPlay(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'limit',
        type=int,
        required=False
    )
    def get(self):
        data = FreeToPlay.parser.parse_args()
        session = StoreDB.session
        result = dict()
        if not data['limit']:
            for item in session.query(Free).all():
                instance = session.query(Catalogue).filter(
                    Catalogue.hashcode == item.hashcode).first()
                result[instance.title] = get_full_info(instance, ['publisher', 'price', 'former_price',
                                                                  'hashcode', 'on_sale',
                                                                  'ps_plus', 'cover'])
        else:
            for item in session.query(Free).limit(data['limit']).all():
                instance = session.query(Catalogue).filter(
                    Catalogue.hashcode == item.hashcode).first()
                result[instance.title] = get_full_info(instance, ['publisher', 'price', 'former_price',
                                                                  'hashcode', 'on_sale',
                                                                  'ps_plus', 'cover'])
        if result:
            return Response(json.dumps(result, ensure_ascii=False), mimetype='application/json')
        return {'Message': 'Nothing found'}, 404



class OnSale(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'limit',
        type=int,
        required=False
    )
    def get(self):
        data = OnSale.parser.parse_args()
        session = StoreDB.session
        result = dict()
        if not data['limit']:
            for instance in session.query(Catalogue).filter(
                    Catalogue.on_sale == True).all():
                result[instance.title] = get_full_info(instance, ['publisher', 'price', 'former_price',
                                                                  'hashcode', 'on_sale',
                                                                  'ps_plus', 'cover'])
        else:
            for instance in session.query(Catalogue).filter(
                    Catalogue.on_sale == True).limit(data['limit']).all():
                result[instance.title] = get_full_info(instance, ['publisher', 'price', 'former_price',
                                                                  'hashcode', 'on_sale',
                                                                  'ps_plus', 'cover'])
        if result:
            return Response(json.dumps(result, ensure_ascii=False), mimetype='application/json')
        return {'Message': 'Nothing found'}, 404



class TopSellers(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'limit',
        type=int,
        required=False
    )
    def get(self):
        data = TopSellers.parser.parse_args()
        session = StoreDB.session
        result = dict()
        if not data['limit']:
            for item in session.query(Tops).all():
                instance = session.query(Catalogue).filter(
                    Catalogue.hashcode == item.hashcode).first()
                result[instance.title] = get_full_info(instance, ['publisher', 'price', 'former_price',
                                                                  'hashcode', 'on_sale',
                                                                  'ps_plus', 'cover'])
        else:
            for item in session.query(Tops).all():
                instance = session.query(Catalogue).filter(
                    Catalogue.hashcode == item.hashcode).first()
                result[instance.title] = get_full_info(instance, ['publisher', 'price', 'former_price',
                                                                  'hashcode', 'on_sale',
                                                                  'ps_plus', 'cover'])
        if result:
            return Response(json.dumps(result, ensure_ascii=False), mimetype='application/json')
        return {'Message': 'Nothing found'}, 404



class ByGenre(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'limit',
        type=int,
        required=False
    )
    parser.add_argument(
        'genre',
        type=str,
        required=True,
        help='This field cannot be left blank'
    )
    parser.add_argument(
        'price',
        type=str,
        required=False
    )

    def get(self):
        data = ByGenre.parser.parse_args()
        try:
            assert data['genre'] in genre_map.keys()
        except AssertionError:
            return {'Message': 'Wrong genre'}, 400
        else:
            result = dict()
            session = StoreDB.session
            tr_genre = genre_map[data['genre']]

            if not data['limit'] and not data['price']:
                instances = session.query(Catalogue).filter(
                    Catalogue.genre.any(tr_genre)).all()

            elif not data['limit'] and data['price']:
                instances = session.query(Catalogue).filter(and_(
                    Catalogue.genre.any(tr_genre),
                    Catalogue.price < data['price'])).all()

            elif data['limit'] and data['price']:
                instances = session.query(Catalogue).filter(and_(
                    Catalogue.genre.any(tr_genre),
                    Catalogue.price < data['price'])).limit(data['limit']).all()

            else:
                instances = session.query(Catalogue).filter(
                    Catalogue.genre.any(tr_genre)).limit(data['limit']).all()

            for instance in instances:
                result[instance.id] = get_full_info(instance, limit_to=[
                    'title', 'publisher', 'hashcode',
                    'price', 'genre', 'description',
                    'ps_plus', 'on_sale', 'cover'])
            if result:
                return Response(json.dumps(result, ensure_ascii=False), mimetype='application/json')
            return {'Message': 'Nothing found'}, 404


class CheaperThan(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'limit',
        type=int,
        required=False
    )
    parser.add_argument(
        'price',
        type=int,
        required=True,
        help='This field cannot be left blank'
    )
    def get(self):
        data = CheaperThan.parser.parse_args()
        result = dict()
        session = StoreDB.session
        if not data['limit']:
            instances = session.query(Catalogue).\
                filter(Catalogue.price < data['price']).all()
        else:
            instances = session.query(Catalogue).\
                filter(Catalogue.price < data['price']).limit(data['limit']).all()
        for instance in instances:
            result[instance.id] = get_full_info(instance, limit_to=[
                'title', 'publisher', 'hashcode',
                'price', 'genre', 'description',
                'ps_plus', 'on_sale', 'cover'])
        if result:
            return Response(json.dumps(result, ensure_ascii=False), mimetype='application/json')
        return {'Message': 'Nothing found'}, 404


class RandomGame(Resource):
    def get(self):
        session = StoreDB.session
        result = dict()
        instance = session.query(Catalogue).order_by(func.random()).first()
        result[instance.title] = get_full_info(instance, limit_to=[
            'title', 'publisher', 'hashcode',
            'price', 'genre', 'description',
            'ps_plus', 'on_sale', 'cover'])
        if result:
            return Response(json.dumps(result, ensure_ascii=False), mimetype='application/json')
        return {'Message': 'Nothing found'}, 404


class QuickSearch(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'title',
        type=str,
        required=True,
        help='This field cannot be left blank'
    )

    def get(self):
        data = QuickSearch.parser.parse_args()
        session = StoreDB.session
        result = dict()
        for instance in session.query(Catalogue).filter(Catalogue.title.ilike(f"%{data['title']}%")).all():
            result[instance.title] = get_full_info(instance, ['publisher', 'price',
                                                              'hashcode', 'on_sale',
                                                              'ps_plus', 'cover'])
        if result:
            return Response(json.dumps(result, ensure_ascii=False), mimetype='application/json')
        return {'Message': f'Nothing found for {data["title"]}'}, 404


class PSPlus(Resource):
    def get(self):
        hashes = requests.get(EXTERNAL['ps plus']).json()['PS Plus Free Games']
        result = dict()
        session = StoreDB.session
        for hashcode in hashes:
            instance = session.query(Catalogue).filter_by(hashcode=hashcode).one()
            result[instance.title] = get_full_info(instance, ['publisher', 'price',
                                                              'hashcode', 'on_sale',
                                                              'ps_plus', 'cover'])
        if result:
            return Response(json.dumps(result, ensure_ascii=False), mimetype='application/json')
        return {'Message': f'Nothing found for {data["title"]}'}, 404


class GameByHashcode(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'hashcode',
        str,
        required=True,
        help='This field cannot be left blank'
    )

    def get(self):
        data = GameByHashcode.parser.parse_args()
        session = StoreDB.session
        item = session.query(Catalogue).filter_by(hashcode=data['hashcode']).one()
        if item:
            return Response(json.dumps(get_full_info(item),
                                       ensure_ascii=False, default=str),
                            mimetype='application/json')
        return {'Message': 'Nothing found'}, 404


api.add_resource(ByGenre, '/api/ru/by-genre')
api.add_resource(CheaperThan, '/api/ru/cheaper')
api.add_resource(RandomGame, '/api/ru/random')
api.add_resource(QuickSearch, '/api/ru/search')
api.add_resource(GameByHashcode, '/api/ru/details')
api.add_resource(PSPlus, '/api/ru/ps-plus')
api.add_resource(OnSale, '/api/ru/on-sale')
api.add_resource(TopSellers, '/api/ru/top-sellers')
api.add_resource(FreeToPlay, '/api/ru/free-to-play')
api.add_resource(ToBeReleased, '/api/ru/upcoming')


if __name__ == '__main__':
    app.run(port=APP_PARAMETERS["port"])
