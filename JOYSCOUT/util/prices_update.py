#!/usr/bin/python3

from sqlalchemy import MetaData, Column, String, Integer, ARRAY, Boolean, DateTime
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.exc import NoResultFound
from threading import Thread
from pu_conf import ps_hits
import requests
import datetime

engine = create_engine('postgresql+psycopg2://joyscout:zetabyte0021@localhost/main', pool_size=40, max_overflow=0)
Base = declarative_base()
metadata = MetaData(engine)

threads = []

class Catalogue(Base):
    __tablename__ = 'catalogue_of_games'
    id = Column(Integer, primary_key=True)
    link = Column(String, unique=True)
    hashcode = Column(String, unique=True)
    title = Column(String)
    publisher = Column(String)
    category = Column(String)
    price = Column(Integer)
    former_price = Column(Integer)
    lowest_price = Column(Integer)
    highest_price = Column(Integer)
    on_sale = Column(Boolean)
    ps_plus = Column(String)
    language = Column(ARRAY(String))
    genre = Column(ARRAY(String))
    size = Column(String)
    age = Column(Integer)
    cover = Column(String)
    screenshots = Column(ARRAY(String))
    description = Column(String)
    updated = Column(DateTime)


class Discounts(Base):
    __tablename__ = 'discounts'
    id = Column(Integer, primary_key=True)
    link = Column(String, unique=True)
    hashcode = Column(String, unique=True)


Session = sessionmaker(bind=engine)
session = scoped_session(Session)
rows = session.query(Discounts)
hashes_in_catalogue = list(set([instance.hashcode for instance in rows]+ps_hits))


def update(hashcode):
    try:
        item = session.query(Catalogue).filter_by(hashcode=hashcode).one()
    except NoResultFound:
        pass
    else:
        print(item.title)
        details = requests.get('http://kuver.chrge.ru:80/api/games/info/%s' % hashcode)
        # If the game exists in PS4 Store
        if not details.status_code == 404:
            json_data = details.json()
            item.on_sale = json_data['details']['on sale']
            item.ps_plus = json_data['details']['PS Plus discount']
            item.price = json_data['details']['price']

            if item.highest_price:
                if json_data['details']['price'] > item.highest_price:
                    item.highest_price = json_data['details']['price']
            else:
                item.highest_price = json_data['details']['price']
            if item.lowest_price:
                if json_data['details']['price'] < item.lowest_price:
                    item.lowest_price = json_data['details']['price']
            elif not item.lowest_price and json_data['details']['on sale']:
                item.lowest_price = json_data['details']['price']

            item.updated = datetime.datetime.now().date()
            session.commit()
        else:
            pass


def begin():
    global threads
    sublist = round(len(hashes_in_catalogue) * 0.1)
    while sublist > 0:
        for hashcode in hashes_in_catalogue[0:sublist]:
            t = Thread(target=update, args=(hashcode,))
            threads.append(t)
        del hashes_in_catalogue[0:sublist]
        sublist = round(len(hashes_in_catalogue) * 0.2)
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()
        threads = []

    if hashes_in_catalogue:
        for hashcode in hashes_in_catalogue:
            t = Thread(target=update, args=(hashcode,))
            threads.append(t)
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()


if __name__ == '__main__':
    begin()
