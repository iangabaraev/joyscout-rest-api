from sqlalchemy import Column, Boolean, String, Integer, DateTime, ARRAY
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
import requests
import hashlib
import threading
import re
from bs4 import BeautifulSoup
import datetime
import sys
sys.path.append('/playground/MEDIA')
import medialib


threads = []


engine = create_engine('postgresql+psycopg2://joyscout:zetabyte0021@localhost/main')
everything = requests.get('http://localhost:80/api/all').json()
hashes = [h for h in everything['all hashes and links'].values()]
Base = declarative_base()


class Item(Base):
    __tablename__ = 'catalogue_of_games'
    id = Column(Integer, primary_key=True)
    link = Column(String, unique=True)
    hashcode = Column(String, unique=True)
    title = Column(String)
    publisher = Column(String)
    category = Column(String)
    price = Column(Integer)
    former_price = Column(Integer)
    lowest_price = Column(Integer)
    highest_price = Column(Integer)
    on_sale = Column(Boolean)
    ps_plus = Column(String)
    language = Column(ARRAY(String))
    genre = Column(ARRAY(String))
    size = Column(String)
    age = Column(Integer)
    cover = Column(String)
    screenshots = Column(ARRAY(String))
    description = Column(String)
    updated = Column(DateTime)


Base.metadata.create_all(engine)


Session = sessionmaker(bind=engine)
session = scoped_session(Session)


#def get_game_title(url):
#    response = requests.get(url)
#    html = response.content
#    soup = BeautifulSoup(html, features='html.parser')
#    title = soup.find("h2", {"class": "pdp__title"}).text
#    return title


def post(hashcode):
    details = requests.get('http://localhost:80/api/games/info/%s' % hashcode).json()
    try:
        print(details['title'])
    except KeyError:
        pass
    item = Item(
        link=details['link'],
        hashcode=hashlib.md5(details['link'].encode("utf-8")).hexdigest(),
        title=details['title'],
        publisher=details['details']['publisher'],
        category=details['details']['category'],
        price=details['details']['price'],
        former_price=details['details']['previous price'],
        on_sale=details['details']['on sale'],
        ps_plus=details['details']['PS Plus discount'],
        language=details['details']['language'],
        genre=details['details']['genre'],
        size=details['details']['size'],
        description=re.sub('\nОписание\n\n|\\t|\\n', '',
                           details['details']['description']),
        updated=datetime.datetime.now().date(),
        age=details['details']['age limit'],
        cover=details['details']['cover'],
        screenshots=details['details']['screenshots'],
    )
    #medialib.MediaLib.save_cover_picture(url=details['details']['cover'],
    #                                     hashcode=hashlib.md5(
    #                                         details['link'].encode("utf-8")).hexdigest())
    #try:
    #    medialib.MediaLib().save_standard_screenshots(cover_url=details['details']['cover'],
    #                                                  hashcode=hashlib.md5(
    #                                         details['link'].encode("utf-8")).hexdigest())
    #except KeyError:
    #    pass
    session.add(item)
    session.commit()


def run_threads(hashes):
    for hashcode in hashes:
        worker = threading.Thread(target=post, args=(hashcode,))
        threads.append(worker)
    for thread in threads:
        thread.start()


#if __name__ == '__main__':
   # for i in range(0, len(hashes)+1, 10):
   #      post(hashes[i:i+10])
   #  for thread in threads:
   #     thread.join()

if __name__ == '__main__':
    for hash in hashes:
        try:
            post(hash)
        except Exception:
            pass
