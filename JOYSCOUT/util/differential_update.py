#!/usr/bin/python3

from sqlalchemy import Table, MetaData, Column, String, Integer, ARRAY, DateTime, Boolean
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
import requests
import datetime
import re
import datetime
import hashlib

engine = create_engine('postgresql+psycopg2://joyscout:zetabyte0021@localhost/main')
Base = declarative_base()
metadata = MetaData(engine)

log = open('/playground/log/differential_update.log', 'a')


class HNL(Base):
    __tablename__ = 'hashes_and_links'
    hashcode = Column(String, primary_key=True)
    link = Column(String, unique=True)


class Catalogue(Base):
    __tablename__ = 'catalogue_of_games'
    id = Column(Integer, primary_key=True)
    link = Column(String, unique=True)
    hashcode = Column(String, unique=True)
    title = Column(String)
    publisher = Column(String)
    category = Column(String)
    price = Column(Integer)
    former_price = Column(Integer)
    lowest_price = Column(Integer)
    highest_price = Column(Integer)
    on_sale = Column(Boolean)
    ps_plus = Column(String)
    language = Column(ARRAY(String))
    genre = Column(ARRAY(String))
    size = Column(String)
    age = Column(Integer)
    cover = Column(String)
    screenshots = Column(ARRAY(String))
    description = Column(String)
    updated = Column(DateTime)


#HNL = Table('hashes_and_links', metadata, autoload=True, autoload_with=engine)
Session = sessionmaker(bind=engine)
session = scoped_session(Session)

res = session.query(Catalogue)
hashes_in_catalogue = [instance.hashcode for instance in res]
_res = session.query(HNL)
hashes_in_mapper = [instance.hashcode for instance in _res]


def what_is_new():
    return list(filter(lambda x: x not in hashes_in_catalogue, hashes_in_mapper))


#def post(hashcode):
#    details = requests.get('http://localhost:80/api/games/info/%s' % hashcode).json()
#    if not details['Message'] == 'Nothing found':
#        try:
#            print(details['title'])
#        except KeyError as e:
#            print(str(e))
#        item = Catalogue(
#            link=str(details['link']),
#            hashcode=hashlib.md5(details['link'].encode("utf-8")).hexdigest(),
#            title=details['title'],
#            publisher=details['details']['publisher'],
#            category=details['details']['category'],
#            price=details['details']['price'],
#            former_price=details['details']['previous price'],
#            on_sale=details['details']['on sale'],
#            ps_plus=details['details']['PS Plus discount'],
#            language=details['details']['language'],
#            genre=details['details']['genre'],
#            size=details['details']['size'],
#            description=re.sub('\nОписание\n\n|\\t|\\n', '',
#                               details['details']['description']),
#            updated=datetime.datetime.now().date(),
#            age=details['details']['age limit'],
#            cover=details['details']['cover'],
#            screenshots=details['details']['screenshots'],
#        )
#        session.add(item)
#        session.commit()
#    else:
#        obj=session.query(HNL).filter_by(hashcode=hashcode).one()
#        session.delete(obj)
#        session.commit()





def post(hashcode):
    details = requests.get('http://localhost:80/api/games/info/%s' % hashcode).json()
    try:
        if details['Message'] == 'Nothing found':
            obj = session.query(HNL).filter_by(hashcode=hashcode).one()
            session.delete(obj)
            session.commit()
        else:
            pass
    except KeyError:
        log.write(details['title']+'\n')
        game_description = None
        if details['details']['description']:
            game_description = re.sub('\nОписание\n\n|\\t|\\n', '',
                               details['details']['description'])
        item = Catalogue(
            link=str(details['link']),
            hashcode=hashlib.md5(details['link'].encode("utf-8")).hexdigest(),
            title=details['title'],
            publisher=details['details']['publisher'],
            category=details['details']['category'],
            price=details['details']['price'],
            former_price=details['details']['previous price'],
            on_sale=details['details']['on sale'],
            ps_plus=details['details']['PS Plus discount'],
            language=details['details']['language'],
            genre=details['details']['genre'],
            size=details['details']['size'],
            description=game_description,
            updated=datetime.datetime.now().date(),
            age=details['details']['age limit'],
            cover=details['details']['cover'],
            screenshots=details['details']['screenshots'],
        )
        session.add(item)
        session.commit()


if __name__ == '__main__':
    log.write(str(datetime.datetime.now())+'\n')
    for hash_code in what_is_new():
        post(hash_code)
    log.close()
