# Production version - joyscout configuration


APP_PARAMETERS = {
    'port': 7000,
    'db': 'postgresql+psycopg2://joyscout:zetabyte0021@localhost/main',
    'lazy update': True,
}

EXTERNAL = {
    'restful specs': 'http://kuver.chrge.ru/api/games/info/',
    'ps plus': 'http://kuver.chrge.ru/api/store/ps-plus-games?hashed=True',
}

INTERNAL = {
    'media folder': '/Users/ian/PycharmProjects/joyscout/MEDIA/COVERS',
}

genre_map = {
    "arcade": "Аркада",
    "horror": "Ужасы",
    "puzzle": "Пазлы",
    "casual": "Казуальные",
    "chill": "Тусовка",
    "strategy": "Стратегия",
    "fps": "Шутер",
    "music": "MUSIC/RHYTHM",
    "unique": "Уникальнык",
    "adventure": "Приключения",
    "racing": "Гонки",
    "family": "Семейные",
    "sport": "Спорт",
    "rpg": "Ролевые игры",
    "action": "Боевик",
    "simulator": "Симуляторы",
    "fighting": "Единоборства",
}
