JOYSCOUT - это первый публичный REST API для русского магазина приложений PlayStation4 Store.
API предоставляет информацию о всех текущих скидках и предложениях, динамике изменения цен, сортировку по названию, жанру, стоимости игры,
и множество других функций. 
