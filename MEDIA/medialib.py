import requests
import os
import re
import hashlib
from bs4 import BeautifulSoup
from media_conf import INTERNAL, EXTERNAL, SELECTORS


class MediaLib:

    @staticmethod
    def ps_store_region_switch(url):
        return re.sub('ru-ru', 'en-us', url)

    @staticmethod
    def http_fy_title(title):
        title = re.sub('\s', '-', title)
        return title.strip(':')

    @staticmethod
    def save_cover_picture(url, hashcode):
        response = requests.get(url, allow_redirects=True)
        open(f"{INTERNAL['cover folder']}{hashcode}.jpeg", "wb").write(response.content)

    @staticmethod
    def save_standard_screenshots(cover_url, hashcode):
        api_url = re.sub('\/image\?.*', '', cover_url)
        json_data = requests.get(api_url).json()
        screenshots = [x["url"] for x in dict(json_data)["mediaList"]["screenshots"]]
        path = INTERNAL['screenshots folder'] + hashcode
        os.mkdir(path)

        for shot in screenshots:
            filename = hashlib.md5(shot.encode('utf-8')).hexdigest()
            response = requests.get(shot, allow_redirects=True)
            open(f"{path}/{filename}.jpg", "wb").write(response.content)

    @staticmethod
    def find_save_screenshots(title, hashcode):

        anchors = []
        views = []

        response = requests.get(EXTERNAL['scr host'] % title)

        if not response.status_code == 404:

            source = response.content
            soup = BeautifulSoup(source, features='html.parser')

            a = soup.find_all('a', SELECTORS['anchors'])

            for url in a:
                anchors.append(EXTERNAL['scr domain'] + url['href'])

            for a in anchors:
                source = requests.get(a).content
                soup = BeautifulSoup(source, features='html.parser')
                img = soup.find('img', SELECTORS['views'])
                views.append('https:' + img['src'])

            path = INTERNAL['screenshots folder'] + hashcode
            os.mkdir(path)

            for view in views:
                filename = hashlib.md5(view.encode('utf-8')).hexdigest()
                response = requests.get(view, allow_redirects=True)
                open(f"{path}/{filename}.jpg", "wb").write(response.content)
