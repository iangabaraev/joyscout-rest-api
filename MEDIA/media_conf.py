INTERNAL = {
    'cover folder': '/playground/MEDIA/COVERS/',
    'screenshots folder': '/playground/MEDIA/SCREENSHOTS/',
}

EXTERNAL = {
    'scr host': 'https://www.gamepressure.com/games/%s/',
    'scr domain': 'https://www.gamepressure.com',
}

SELECTORS = {
    'anchors': {'title': 'Screenshot'},
    'views': {'id': 'screenshot'},
}
