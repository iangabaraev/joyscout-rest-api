#!/usr/bin/python3

from flask import Flask
from flask import Response, request
from flask_restful import Api, Resource, reqparse
from sqlalchemy import Column, Boolean, String, Integer, DateTime, ARRAY
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from discounts_conf import APP_PARAMETERS
import sys
sys.path.append('/playground/STORE')
from playstation_homepage import PlayStationStoreWebsite
import hashlib
import datetime

log = open('/playground/log/discounts_update.log', 'a')

app = Flask(__name__)
api = Api(app)
engine = create_engine(APP_PARAMETERS['db'], echo=False)
Base = declarative_base()


class Discounts(Base):
    __tablename__ = 'discounts'
    id = Column(Integer, primary_key=True)
    link = Column(String, unique=True)
    hashcode = Column(String, unique=True)


Base.metadata.create_all(engine)
Discounts.__table__.drop(engine)
Base.metadata.create_all(engine)


def update():
    Session = sessionmaker(bind=engine)
    session = Session()
    all_links = PlayStationStoreWebsite().get_all_discounts_links()
    top_sellers = PlayStationStoreWebsite().get_top_sellers()
    all_links.update(top_sellers)
    for link in all_links:
        item = Discounts(
            link=link,
            hashcode=hashlib.md5(link.encode("utf-8")).hexdigest())
        session.add(item)
    session.commit()

try:
    update()
except Exception as e:
    log.write(str(datetime.datetime.now())+': '+str(e)+'\n')
else:
    log.write(str(datetime.datetime.now())+': '+'Updated\n')
