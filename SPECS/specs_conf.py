APP_PARAMETERS = {
    "port": 5007,
}

EXTERNAL = {
    "mapper": "http://localhost:80/api/hash-to-link?hashcode=",
}

SPECS = {
    "languages": ['немецкий', 'норвежский', 'финский', 'шведский',
                          'русский', 'португальский', 'английский', 'итальянский',
                          'французский', 'испанский', 'арабский', 'польский',
                          'датский', 'Турецкий', 'нидерландский', 'китайский (упрощ. письмо)'],

    "genres": ['Боевик', 'Приключения', 'Гонки', 'Аркада', 'Ужасы', 'Пазлы',
                       'Симуляторы', 'Спорт', 'Семейные', 'Казуальные',
                       'Ролевые игры', 'Тусовка', 'Стратегия',
                       'Единоборства', 'Шутер', 'MUSIC/RHYTHM', 'Уникальные']
}

SELECTORS = {
    "cover": "div.large-3 > div.pdp__"
             "thumbnail-img > div.product-image > "
             "div.product-image__img > "
             "div.product-image__img--main > img",
    "title": {"class": "pdp__title"},
    "publisher": {"class": "provider-info__text"},
    "category": {"class": "provider-info__list-item"},
    "price": {"class": "price-display__price"},
    "previous price":  {"class": "price-display__strikethrough"},
    "psplus discount": {"class": "price-display__price__label"},
    "specs": {"class": "tech-specs__pivot-menus"},
    "description": {"class": "pdp__description"},
}
