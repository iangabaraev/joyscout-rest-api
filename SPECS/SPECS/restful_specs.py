from flask import Flask, Response
from flask import jsonify
import requests
import re
import json
import time
from SPECS.conf import APP_PARAMETERS, EXTERNAL, SPECS, SELECTORS
from bs4 import BeautifulSoup


app = Flask(__name__)
restful_mapper_URI = EXTERNAL["mapper"]


class PS4Game:
    def __init__(self, url):
        self.url = url
        self.languages = SPECS["languages"]
        self.genres = SPECS["genres"]
        self.title = None
        self.publisher = None
        self.category = None
        self.price = 0
        self.previous_price = 0
        self.on_sale = False
        self.psplus_discount = False
        self.lang = None
        self.genre = None
        self.size = None
        self.description = None
        self.cover = None

    @classmethod
    def find_by_hashcode(cls, hashcode):
        response = requests.get(url=restful_mapper_URI + hashcode).json()
        return cls(response['link'])

    def gather_game_info(self):
        response = requests.get(self.url.encode("utf-8").decode("utf-8-sig"))
        html = response.content
        soup = BeautifulSoup(html, features='html.parser')

        # Game title
        try:
            self.title = soup.find("h2", SELECTORS["title"]).text
        except AttributeError:
            time.sleep(60)

        # Game publisher
        self.publisher = soup.find("h5", SELECTORS["publisher"]).text
        if '\n' in self.publisher:
            self.publisher = None

        # Game category
        try:
            self.category = soup.find("span", SELECTORS["category"]).text
        except AttributeError:
            pass

        # Current price
        try:
            self.price = int(re.sub('\D', '',
                                soup.find("h3",
                                          SELECTORS["price"]).text))
        except ValueError:
            pass
        except AttributeError:
            pass

        # Former price
        try:
            self.previous_price = int(re.sub('\D', '',
                                             soup.find("span",
                                                       SELECTORS["previous price"]).text))
        except AttributeError:
            pass
        else:
            self.on_sale = True

        # PS Plus discount
        try:
            self.psplus_discount = soup.find("div",
                                             SELECTORS["psplus discount"]).text
        except AttributeError:
            pass

        try:
            cover_picture_url = soup.select(SELECTORS["cover"])
            if cover_picture_url:
                self.cover = cover_picture_url[0]['src']
        except AttributeError:
            pass

        # Other specs
        try:
            specs = list(map(lambda x: x.strip(),
                         soup.find("div", SELECTORS["specs"]).text.split('\n')))
            self.lang = set(filter(lambda l: l in self.languages, specs))
            # Game genre
            self.genre = set(filter(lambda g: g in self.genres, specs))
            # Game file size
            try:
                self.size = " ".join(specs[specs.index('Размер файла'):])
            except ValueError:
                pass
            self.description = soup.find("div", SELECTORS["description"]).text
        except AttributeError:
            pass

    # Retrieve results in JSON
    def json(self):
        self.gather_game_info()
        description = {
            "title": self.title,
            "link": self.url,
            "details": {
                "publisher": self.publisher,
                "category": self.category,
                "price": self.price,
                "previous price": self.previous_price,
                "on sale": self.on_sale,
                "PS Plus discount": self.psplus_discount,
                "size": self.size,
                "description": self.description,
                "cover": self.cover,
            }
        }
        if self.lang is not None:
            description['details']['language'] = ",".join(self.lang)
        else:
            description['details']['language'] = self.lang
        if self.genre is not None:
            description['details']['genre'] = ",".join(self.genre)
        else:
            description['details']['genre'] = self.genre
        return json.dumps(description, ensure_ascii=False, indent=4)


@app.route('/api/games/info/<hashcode>')
def game_info_by_hash(hashcode):
    try:
        return Response(PS4Game.find_by_hashcode(hashcode).json(), mimetype='application/json')
    except Exception as e:
        return jsonify({'Message': 'Something went wrong'}), 500


app.run(port=APP_PARAMETERS["port"])
