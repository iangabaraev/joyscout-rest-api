#!/usr/bin/python3

from restful_mapper import *
import datetime

log = open('/playground/log/mapper_update.log', 'a')

def put():
    data = {'lazy': 1}
    try:
        if data['lazy']:
            all_ps4store_links = PlayStationStoreWebsite().get_all_new_games_links()
        else:
            all_ps4store_links = PlayStationStoreWebsite().get_all_links()

        all_local_links = Links().all
        everything_new = [entry for entry in all_ps4store_links if entry not in all_local_links]

        if len(everything_new) == 0:
            log.write(str(datetime.datetime.now())+': Database is up-to-date\n')
        else:
            for link in everything_new:
                item = MainCatalogue(hashcode=md5(link.encode("utf-8")
                                                  ).hexdigest().encode("utf-8"
                                                                       ).decode("utf-8-sig"
                                                                                ), link=link)
                session.add(item)
            session.commit()
    except Exception as e:
        log.write(str(e)+'\n')
    else:
        log.write(str(datetime.datetime.now())+': Database updated\n')


#if __name__ == '__main__':
put()
log.close()
