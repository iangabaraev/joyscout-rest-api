from flask import Flask, Response
from sqlalchemy import Column, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from flask_restful import Resource, Api, reqparse
from hashlib import md5
import json
from conf import APP_PARAMETERS

import sys
sys.path.append('/playground/STORE')
from playstation_homepage import PlayStationStoreWebsite

app = Flask(__name__)
api = Api(app)

engine = create_engine(APP_PARAMETERS['db'], echo=False)
Base = declarative_base()
threads = []


class MainCatalogue(Base):
    __tablename__ = 'hashes_and_links'
    hashcode = Column(String, unique=True, primary_key=True)
    link = Column(String, unique=True)


Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()


class Links:
    def __init__(self):
        self.all = set()
        for instance in session.query(MainCatalogue).all():
            self.all.add(instance.link)


class Hashes:
    def __init__(self):
        self.all = set()
        for instance in session.query(MainCatalogue).all():
            self.all.add(instance.hashcode)


class All(Resource):
    def get(self):
        links = Links().all
        hashes = Hashes().all
        result = dict(zip(links, hashes))
        return Response(json.dumps({'all hashes and links': result},
                                   ensure_ascii=False), mimetype='application/json')


class Update(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'lazy',
        str,
        required=False
    )
    def put(self):
        data = Update.parser.parse_args()
        try:
            if data['lazy']:
                all_ps4store_links = PlayStationStoreWebsite().get_all_new_games_links()
            else:
                all_ps4store_links = PlayStationStoreWebsite().get_all_links()

            all_local_links = Links().all
            everything_new = [entry for entry in all_ps4store_links if entry not in all_local_links]

            if len(everything_new) == 0:
                return {'Message': 'Database is up-to-date'}, 200
            else:
                for link in everything_new:
                    item = MainCatalogue(hashcode=md5(link.encode("utf-8")
                                                  ).hexdigest().encode("utf-8"
                                                                       ).decode("utf-8-sig"
                                                                                ), link=link)
                    session.add(item)
                session.commit()
        except Exception as e:
            return {'Message': str(e)}, 500
        else:
            return {'Message': 'Database updated'}, 200


# Takes the hash, gives the link
class Hash2Link(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('hashcode',
                        type=str,
                        required=True,
                        help="This field cannot be left blank!"
                        )

    def get(self):
        data = Hash2Link.parser.parse_args()
        link = session.query(MainCatalogue).filter_by(hashcode=data['hashcode']).first()
        if link:
            return Response(json.dumps({'link': link.link},
                                       ensure_ascii=False), mimetype='application/json')
        return {'Message': 'Nothing found'}, 404


api.add_resource(Update, '/api/update-database')
api.add_resource(Hash2Link, '/api/hash-to-link')
api.add_resource(All, '/api/all')

if __name__ == '__main__':
    #Update().get()
    app.run(port=APP_PARAMETERS['port'])
